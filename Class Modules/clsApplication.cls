VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsApplication"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Written by : Daniel Shuy

Option Explicit

Private mstrPathTemplate As String

Private WithEvents mappWord As Application
Attribute mappWord.VB_VarHelpID = -1

Friend Sub Constructor(ByVal strPathTemplate_IN As String)
    mstrPathTemplate = strPathTemplate_IN
    
    Set mappWord = Application
End Sub

Private Sub mappWord_DocumentOpen(ByVal doc_IN As Document)
    Dim ltmp As Template
    
    Set ltmp = doc_IN.AttachedTemplate
    
    If ltmp.FullName = mstrPathTemplate Then
        Call basMain.setDocument(doc_IN)
    End If
End Sub

Private Sub mappWord_DocumentBeforeSave(ByVal doc_IN As Document, ByRef blnSaveAsUI_OUT As Boolean, ByRef blnCancel_OUT As Boolean)
    Dim lclsDocument As clsDocument
    
    Set lclsDocument = basMain.getDocument(doc_IN)
    If Not lclsDocument Is Nothing Then
        Call lclsDocument.getAutoUpdateTableOfContents.UpdateTableOfContents
    End If
End Sub

Private Sub mappWord_NewDocument(ByVal doc_IN As Document)
    Dim ltmp As Template
    
    Set ltmp = doc_IN.AttachedTemplate
    
    If ltmp.FullName = mstrPathTemplate Then
        Call basMain.setDocument(doc_IN)
    End If
End Sub
