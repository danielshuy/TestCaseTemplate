VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "itfResizePicture"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Written by : Daniel Shuy

Option Explicit

Public Sub ConvertPBrushToPicture()
End Sub

Public Sub Delete()
End Sub

Public Function shpDuplicate(ByVal strTitleExpand_IN As String) As Shape
End Function

Public Sub ShrinkToThumbnail(ByVal sngThumbnailWidth_IN As Single)
End Sub

Public Function blnTitleEquals(ByVal strTitle_IN As String) As Boolean
End Function
