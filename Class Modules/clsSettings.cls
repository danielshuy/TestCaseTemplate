VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsSettings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Written by : Daniel Shuy

Option Explicit

Private mclsDocument As clsDocument

Private mblnModified As Boolean

' Variables to store unsaved Settings
Private mblnAutoUpdateTableOfContents As Boolean
Private mdlbShrinkToThumbnailWidth As Double

Friend Sub Constructor(ByRef clsDocument_INOUT As clsDocument)
    Set mclsDocument = clsDocument_INOUT
    
    mblnModified = False
End Sub

Friend Property Get getModified() As Boolean
    getModified = mblnModified
End Property

Friend Property Let setModified(ByVal blnModified_IN As Boolean)
    mblnModified = blnModified_IN
End Property

Friend Property Get getAutoUpdateTableOfContents() As Boolean
    getAutoUpdateTableOfContents = mblnAutoUpdateTableOfContents
End Property

Friend Property Let setAutoUpdateTableOfContents(ByVal blnAutoUpdateTableOfContents_IN As Boolean)
    mblnAutoUpdateTableOfContents = blnAutoUpdateTableOfContents_IN
End Property

Friend Property Get getShrinkToThumbnailWidth() As Double
    getShrinkToThumbnailWidth = mdlbShrinkToThumbnailWidth
End Property

Friend Property Let setShrinkToThumbnailWidth(ByVal dblShrinkToThumbnailWidth_IN As Double)
    mdlbShrinkToThumbnailWidth = dblShrinkToThumbnailWidth_IN
End Property
