VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsDocument"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Written by : Daniel Shuy

Option Explicit

Private mclsSettings As clsSettings
Private mclsAutoUpdateTableOfContents As clsAutoUpdateTableOfContents
Private mclsResizePicture As clsResizePicture

Public WithEvents mdoc As Document
Attribute mdoc.VB_VarHelpID = -1

Friend Sub Constructor(ByRef doc_INOUT As Document)
    Set mdoc = doc_INOUT
    
    Set mclsSettings = basConstructors.newSettings(Me)
    Set mclsAutoUpdateTableOfContents = basConstructors.newAutoUpdateTableOfContents(Me)
    Set mclsResizePicture = basConstructors.newResizePicture(Me)
End Sub

Friend Property Get getSettings() As clsSettings
    Set getSettings = mclsSettings
End Property

Friend Property Get getAutoUpdateTableOfContents() As clsAutoUpdateTableOfContents
    Set getAutoUpdateTableOfContents = mclsAutoUpdateTableOfContents
End Property

Friend Property Get getResizePicture() As clsResizePicture
    Set getResizePicture = mclsResizePicture
End Property
