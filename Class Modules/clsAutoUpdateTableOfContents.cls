VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsAutoUpdateTableOfContents"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Written by : Daniel Shuy

Option Explicit

Private Const mblnDEFAULT_ENABLED As Boolean = True
Private Const mstrDOCUMENT_VARIABLES_KEY_AUTO_UPDATE_TABLE_OF_CONTENTS As String = "blnAutoUpdateTableOfContents"

Private mclsDocument As clsDocument

Friend Sub Constructor(ByRef clsDocument_INOUT As clsDocument)
    Set mclsDocument = clsDocument_INOUT
    
    If Not basUtilities.DocumentVariables_blnContains(mclsDocument.mdoc, mstrDOCUMENT_VARIABLES_KEY_AUTO_UPDATE_TABLE_OF_CONTENTS) Then
        Call mclsDocument.mdoc.Variables.Add(mstrDOCUMENT_VARIABLES_KEY_AUTO_UPDATE_TABLE_OF_CONTENTS, mblnDEFAULT_ENABLED)
    End If
End Sub

Friend Sub ResetToDefaults()
    setEnabled = mblnDEFAULT_ENABLED
End Sub

Friend Sub UpdateTableOfContents()
    Dim lblnScreenUpdating As Boolean
    Dim lund As UndoRecord
    Dim ltoc As TableOfContents
    
    ' If disabled in Settings
    If Not isEnabled Then
        Exit Sub
    End If
    
    lblnScreenUpdating = Application.ScreenUpdating ' store previous ScreenUpdating flag
    Application.ScreenUpdating = False
    
    Set lund = Application.UndoRecord
    
    Call lund.StartCustomRecord("Update Table of Contents")
    
    For Each ltoc In mclsDocument.mdoc.TablesOfContents
        Call ltoc.Update
    Next ltoc
    
    Call lund.EndCustomRecord
    
    Application.ScreenUpdating = lblnScreenUpdating ' restore previous ScreenUpdating flag
End Sub

Friend Property Get isEnabled() As Boolean
    Dim lReturn As Boolean
    
    lReturn = mclsDocument.mdoc.Variables(mstrDOCUMENT_VARIABLES_KEY_AUTO_UPDATE_TABLE_OF_CONTENTS).Value
    
    isEnabled = lReturn
End Property

Friend Property Let setEnabled(ByVal blnEnabled_IN As Boolean)
    mclsDocument.mdoc.Variables(mstrDOCUMENT_VARIABLES_KEY_AUTO_UPDATE_TABLE_OF_CONTENTS).Value = blnEnabled_IN
End Property
