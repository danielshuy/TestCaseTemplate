VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsResizePicture"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Written by : Daniel Shuy

Option Explicit

Private Const mdblDEFAULT_THUMBNAIL_WIDTH As Double = 2.65  ' in CM

Private Const mstrDOCUMENT_VARIABLES_KEY_THUMBNAIL_WIDTH As String = "sngThumbnailWidth"

Private Const mstrTITLE_SHAPE_EXPAND As String = "shpExpand"
Private Const mstrTITLE_SHAPE_MASK As String = "shpMask"
Private Const mstrDOCUMENT_VARIABLES_KEY_ZOOM_PERCENTAGE As String = "lngZoomPercentage"

Private mclsDocument As clsDocument
Private mdicInlineShape As New Scripting.Dictionary
Private mdicShape As New Scripting.Dictionary

Friend Sub Constructor(ByRef clsDocument_INOUT As clsDocument)
    Set mclsDocument = clsDocument_INOUT
    
    If Not basUtilities.DocumentVariables_blnContains(mclsDocument.mdoc, mstrDOCUMENT_VARIABLES_KEY_THUMBNAIL_WIDTH) Then
        Call mclsDocument.mdoc.Variables.Add(mstrDOCUMENT_VARIABLES_KEY_THUMBNAIL_WIDTH, CentimetersToPoints(mdblDEFAULT_THUMBNAIL_WIDTH))
    End If
End Sub

Friend Sub ResetToDefaults()
    setThumbnailWidth = mdblDEFAULT_THUMBNAIL_WIDTH
End Sub

Friend Sub ShrinkToThumbnail(Optional ByVal blnCheckIfProcessing_IN As Boolean = True)
    Dim ldicPicture As New Scripting.Dictionary
    Dim lils As InlineShape
    Dim lshp As Shape
    Dim lblnScreenUpdating As Boolean
    Dim lsngThumbnailWidth As Single
    Dim lvntKey As Variant
    
    With Application.Selection
        If .InlineShapes.Count <> 0 Then
            For Each lils In .InlineShapes
                Call ldicPicture.Add(lils, Nothing)
            Next lils
        End If
        
        If .ShapeRange.Count <> 0 Then
            For Each lshp In .ShapeRange
                Call ldicPicture.Add(lshp, Nothing)
            Next lshp
        End If
    End With
    
    ' Do nothing if Dictionary is empty
    If ldicPicture.Count = 0 Then
        Exit Sub
    End If
    
    lsngThumbnailWidth = getThumbnailWidth  ' cache result
    
    lblnScreenUpdating = Application.ScreenUpdating ' store previous ScreenUpdating flag
    Application.ScreenUpdating = False
    
    Do Until ldicPicture.Count = 0
        For Each lvntKey In ldicPicture.Keys
            If blnShrinkToThumbnail(lvntKey, lsngThumbnailWidth, blnCheckIfProcessing_IN) Then
                Call ldicPicture.Remove(lvntKey)
            End If
        Next lvntKey
    Loop
    
    Application.ScreenUpdating = lblnScreenUpdating ' restore previous ScreenUpdating flag
End Sub

Private Function blnShrinkToThumbnail(ByRef lvntPicture_INOUT As Variant, ByVal sngThumbnailWidth_IN As Single, ByVal blnCheckIfProcessing_IN As Boolean) As Boolean
    Dim lReturn As Boolean
    lReturn = True
    
    Dim ldicProcessing As Scripting.Dictionary
    Dim lils As InlineShape
    Dim lshp As Shape
    Dim lclsResizePicture As itfResizePicture
    Dim lund As UndoRecord
    
    If TypeOf lvntPicture_INOUT Is InlineShape Then
        Set ldicProcessing = mdicInlineShape
        Set lils = lvntPicture_INOUT
        Set lclsResizePicture = basConstructors.newResizePictureInlineShape(lils)
    ElseIf TypeOf lvntPicture_INOUT Is Shape Then
        Set ldicProcessing = mdicShape
        Set lshp = lvntPicture_INOUT
        Set lclsResizePicture = basConstructors.newResizePictureShape(lshp)
    Else
        lReturn = False
    End If
    
    If blnCheckIfProcessing_IN Then
        If ldicProcessing.Exists(lvntPicture_INOUT) Then
            lReturn = False
        End If
    End If
    
    If lReturn Then
        Call basUtilities.Dictionary_AddIfNotExist(ldicProcessing, lvntPicture_INOUT, Nothing)
        
        Set lund = Application.UndoRecord
        Call lund.StartCustomRecord("Shrink to Thumbnail")
        
        Call lclsResizePicture.ShrinkToThumbnail(sngThumbnailWidth_IN)
        
        Call lund.EndCustomRecord
        
        Call basUtilities.Dictionary_RemoveIfExist(ldicProcessing, lvntPicture_INOUT)
    End If
    
    blnShrinkToThumbnail = lReturn
End Function

Friend Sub ExpandToOriginalSize(Optional ByVal blnCheckIfProcessing_IN As Boolean = True)
    Dim ldicProcessing As Scripting.Dictionary
    Dim lils As InlineShape
    Dim lshp As Shape
    Dim lvntPicture As Variant
    Dim lclsResizePicture As itfResizePicture
    Dim lblnScreenUpdating As Boolean
    Dim lund As UndoRecord
    
    Dim lshpExpanded As Shape
    
    With Application.Selection
        If .InlineShapes.Count = 1 Then
            Set ldicProcessing = mdicInlineShape
            Set lils = .InlineShapes(1)
            Set lvntPicture = lils
            Set lclsResizePicture = basConstructors.newResizePictureInlineShape(lils)
        ElseIf .ShapeRange.Count = 1 Then
            Set ldicProcessing = mdicShape
            Set lshp = .ShapeRange(1)
            Set lvntPicture = lshp
            Set lclsResizePicture = basConstructors.newResizePictureShape(lshp)
        Else
            Exit Sub
        End If
    End With
    
    If blnCheckIfProcessing_IN Then
        Do While ldicProcessing.Exists(lvntPicture)
            ' pause until Picture is ready
        Loop
    End If
    
    lblnScreenUpdating = Application.ScreenUpdating ' store previous ScreenUpdating flag
    Application.ScreenUpdating = False
    
    Call basUtilities.Dictionary_AddIfNotExist(ldicProcessing, lvntPicture, Nothing)
    
    Call CancelExpand(False)
    
    Set lund = Application.UndoRecord
    Call lund.StartCustomRecord("Expand to Original Size")
    
    Set lshpExpanded = lclsResizePicture.shpDuplicate(mstrTITLE_SHAPE_EXPAND)
    
    Call basUtilities.Dictionary_AddIfNotExist(ldicProcessing, lshpExpanded, Nothing)
    
    With lshpExpanded
        .LockAspectRatio = True
        
        ' set to 100% of original size
        Call .ScaleHeight(1, True)
        Call .ScaleWidth(1, True)
        
        ' FIX: Must be last else may fail due to Command Failed Error (Run-time Error 4198)
        .WrapFormat.Type = wdWrapFront  ' Layout Option: In Front of Text
    End With
    
    Call CenterPicture(lshpExpanded)
    
    Call lund.EndCustomRecord
    
    Call basUtilities.Dictionary_RemoveIfExist(ldicProcessing, lvntPicture)
    Call basUtilities.Dictionary_RemoveIfExist(ldicProcessing, lshpExpanded)
    
    Application.ScreenUpdating = lblnScreenUpdating ' restore previous ScreenUpdating flag
End Sub

Friend Sub ExpandToFitPage(Optional ByVal blnCheckIfProcessing_IN As Boolean = True)
    Dim ldicProcessing As Scripting.Dictionary
    Dim lils As InlineShape
    Dim lshp As Shape
    Dim lvntPicture As Variant
    Dim lclsResizePicture As itfResizePicture
    Dim lblnScreenUpdating As Boolean
    Dim lund As UndoRecord
    Dim lshpExpanded As Shape
    Dim lsngRotation As Single
    Dim ldblTheta As Double
    Dim ldblAspectRatio As Double
    Dim ldblCos As Double
    Dim ldblSin As Double
    Dim lsngWidth1 As Single
    Dim lsngWidth2 As Single
    Dim lsngWidth As Single
    Dim lsngHeight1 As Single
    Dim lsngHeight2 As Single
    Dim lsngHeight As Single
    Dim ldoc As Document
    Dim lwpf As WdPageFit
    
    With Application.Selection
        If .InlineShapes.Count = 1 Then
            Set ldicProcessing = mdicInlineShape
            Set lils = .InlineShapes(1)
            Set lvntPicture = lils
            Set lclsResizePicture = basConstructors.newResizePictureInlineShape(lils)
        ElseIf .ShapeRange.Count = 1 Then
            Set ldicProcessing = mdicShape
            Set lshp = .ShapeRange(1)
            Set lvntPicture = lshp
            Set lclsResizePicture = basConstructors.newResizePictureShape(lshp)
        Else
            Exit Sub
        End If
    End With
    
    If blnCheckIfProcessing_IN Then
        Do While ldicProcessing.Exists(lvntPicture)
            ' pause until Picture is ready
        Loop
    End If
    
    lblnScreenUpdating = Application.ScreenUpdating ' store previous ScreenUpdating flag
    Application.ScreenUpdating = False
    
    Call CancelExpand
    
    Set lund = Application.UndoRecord
    Call lund.StartCustomRecord("Expand to Fit Page")
    
    Call basUtilities.Dictionary_AddIfNotExist(ldicProcessing, lvntPicture, Nothing)
    
    Set lshpExpanded = lclsResizePicture.shpDuplicate(mstrTITLE_SHAPE_EXPAND)
    
    Call basUtilities.Dictionary_AddIfNotExist(ldicProcessing, lshpExpanded, Nothing)
    
    With lshpExpanded
        .LockAspectRatio = True
        .Fill.BackColor = RGB(255, 255, 255)    ' set Transparent
        
        lsngRotation = .Rotation
        If lsngRotation = 0 Then
            .Width = Application.Selection.PageSetup.PageWidth
            If .Height > Application.Selection.PageSetup.PageHeight Then    ' FIX: to handle Portrait Pictures
                .Height = Application.Selection.PageSetup.PageHeight
            End If
        Else    ' if Picture is Rotated
            If lsngRotation > 180 Then
                lsngRotation = 360 - lsngRotation
            End If
            
            ldblTheta = basUtilities.Math_dblDegToRad(lsngRotation)
            
            ldblAspectRatio = .Height / .Width
            
            ldblCos = Math.Cos(ldblTheta)
            ldblSin = Math.Sin(ldblTheta)
            
            lsngWidth1 = Application.Selection.PageSetup.PageHeight / (ldblSin + (ldblAspectRatio * ldblCos))
            lsngWidth2 = Application.Selection.PageSetup.PageWidth / (ldblCos + (ldblAspectRatio * ldblSin))
            
            lsngWidth = basUtilities.Math_dblMin(lsngWidth1, lsngWidth2)
            
            .Width = lsngWidth
            
            ldblAspectRatio = 1 / ldblAspectRatio   ' flip to width/height
            
            lsngHeight1 = Application.Selection.PageSetup.PageHeight / ((ldblAspectRatio * ldblSin) + ldblCos)
            lsngHeight2 = Application.Selection.PageSetup.PageWidth / ((ldblAspectRatio * ldblCos) + ldblSin)
            
            lsngHeight = basUtilities.Math_dblMin(lsngHeight1, lsngHeight2)
            
            If .Height > lsngHeight Then
                .Height = lsngHeight
            End If
        End If
        
        ' FIX: Must be last else may fail due to Command Failed Error (Run-time Error 4198)
        .WrapFormat.Type = wdWrapFront  ' Layout Option: In Front of Text
    End With
    
    Call CenterPicture(lshpExpanded)
    
    Set ldoc = mclsDocument.mdoc
    
    ' store previous Zoom Percentage
    Call ldoc.Variables.Add(mstrDOCUMENT_VARIABLES_KEY_ZOOM_PERCENTAGE, ldoc.ActiveWindow.View.Zoom.Percentage)
    
    With ldoc.ActiveWindow.View.Zoom
        lwpf = .PageFit   ' store previous PageFit settings
        .PageFit = wdPageFitFullPage  ' zoom in/out until entire page can fully fit screen
        .PageFit = lwpf   ' restore previous PageFit settings
    End With
    
    Call lund.EndCustomRecord
    
    Call basUtilities.Dictionary_RemoveIfExist(ldicProcessing, lvntPicture)
    Call basUtilities.Dictionary_RemoveIfExist(ldicProcessing, lshpExpanded)
    
    Application.ScreenUpdating = lblnScreenUpdating ' restore previous ScreenUpdating flag
End Sub

Friend Sub CancelExpand(Optional ByVal blnCheckIfProcessing_IN As Boolean = True)
    Dim ldoc As Document
    Dim lils As InlineShape
    Dim lshp As Shape
    Dim lclsResizePicture As itfResizePicture
    Dim ldicProcessing As Scripting.Dictionary
    Dim lvntPicture As Variant
    Dim lblnScreenUpdating As Boolean
    Dim lund As UndoRecord
    
    Set lvntPicture = Nothing
    
    Set ldoc = mclsDocument.mdoc
    
    For Each lils In ldoc.InlineShapes
        Set lclsResizePicture = basConstructors.newResizePictureInlineShape(lils)
        If lclsResizePicture.blnTitleEquals(mstrTITLE_SHAPE_EXPAND) Then
            Set ldicProcessing = mdicInlineShape
            Set lvntPicture = lils
            Exit For
        End If
    Next lils
    
    If lvntPicture Is Nothing Then
        For Each lshp In ldoc.Shapes
            Set lclsResizePicture = basConstructors.newResizePictureShape(lshp)
            If lclsResizePicture.blnTitleEquals(mstrTITLE_SHAPE_EXPAND) Then
                Set ldicProcessing = mdicShape
                Set lvntPicture = lshp
                Exit For
            End If
        Next lshp
    End If
    
    lblnScreenUpdating = Application.ScreenUpdating ' store previous ScreenUpdating flag
    Application.ScreenUpdating = False
    
    If Not lvntPicture Is Nothing Then
        If blnCheckIfProcessing_IN Then
            Do While ldicProcessing.Exists(lvntPicture)
                ' pause until Picture is ready
            Loop
        End If
        
        Set lund = Application.UndoRecord
        Call lund.StartCustomRecord("Cancel Fit to Page")
        
        Call lclsResizePicture.Delete
        
        Call lund.EndCustomRecord
    End If
    
    If basUtilities.DocumentVariables_blnContains(mclsDocument.mdoc, mstrDOCUMENT_VARIABLES_KEY_ZOOM_PERCENTAGE) Then
        ' restore previous Zoom Percentage
        ldoc.ActiveWindow.View.Zoom.Percentage = mclsDocument.mdoc.Variables(mstrDOCUMENT_VARIABLES_KEY_ZOOM_PERCENTAGE).Value
        Call ldoc.Variables(mstrDOCUMENT_VARIABLES_KEY_ZOOM_PERCENTAGE).Delete
    End If
    
    Application.ScreenUpdating = lblnScreenUpdating ' restore previous ScreenUpdating flag
End Sub

Friend Function blnExpanded(ByVal vntPicture_IN) As Boolean
    Dim lReturn As Boolean
    lReturn = False
    
    Dim lils As InlineShape
    Dim lshp As Shape
    Dim lclsResizePicture As itfResizePicture
    
    If TypeOf vntPicture_IN Is InlineShape Then
        Set lils = vntPicture_IN
        Set lclsResizePicture = basConstructors.newResizePictureInlineShape(lils)
        lReturn = True
    ElseIf TypeOf vntPicture_IN Is Shape Then
        Set lshp = vntPicture_IN
        Set lclsResizePicture = basConstructors.newResizePictureShape(lshp)
        lReturn = True
    End If
    
    If lReturn Then
        lReturn = lclsResizePicture.blnTitleEquals(mstrTITLE_SHAPE_EXPAND)
    End If
    
    blnExpanded = lReturn
End Function

Friend Property Get getThumbnailWidth() As Single
    Dim lReturn As Single
    
    lReturn = mclsDocument.mdoc.Variables(mstrDOCUMENT_VARIABLES_KEY_THUMBNAIL_WIDTH).Value
    
    getThumbnailWidth = lReturn
End Property

Friend Property Let setThumbnailWidth(ByVal dblThumbnailWidth_IN As Double)
    mclsDocument.mdoc.Variables(mstrDOCUMENT_VARIABLES_KEY_THUMBNAIL_WIDTH).Value = CentimetersToPoints(dblThumbnailWidth_IN)
End Property

Private Sub CenterPicture(ByRef shpPicture_INOUT As Shape)
    Call shpPicture_INOUT.Select
    
    With Application.CommandBars
        Call .ExecuteMso("ObjectsAlignRelativeToContainerSmart")
        Call .ExecuteMso("ObjectsAlignCenterHorizontalSmart")
        Call .ExecuteMso("ObjectsAlignMiddleVerticalSmart")
    End With
End Sub

