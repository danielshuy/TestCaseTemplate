VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "clsResizePictureInlineShape"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Written by : Daniel Shuy

Option Explicit
Implements itfResizePicture

Private milsPicture As InlineShape

Friend Sub Constructor(ByRef ilsPicture_INOUT As InlineShape)
    Set milsPicture = ilsPicture_INOUT
End Sub

Friend Sub itfResizePicture_ConvertPBrushToPicture()
    Dim lstrOLEFormatClassType As String
    Dim lrngInlineShape As Range
    Dim lblnScreenUpdating As Boolean
    Dim lilsInlineShapes As InlineShapes
    Dim lilsUnlinked As InlineShape
    Dim lrngUnlinked As Range
    
    With milsPicture
On Error GoTo ProcCatch
        lstrOLEFormatClassType = .OLEFormat.ClassType
On Error GoTo 0
        
        If lstrOLEFormatClassType <> "PBrush" Then
            GoTo ProcFinally
        End If
        
        Set lrngInlineShape = .Range
    End With
    
    lblnScreenUpdating = Application.ScreenUpdating ' store previous ScreenUpdating flag
    Application.ScreenUpdating = False
    
    With lrngInlineShape
        Call .Fields.Unlink
        
        Set lilsInlineShapes = .InlineShapes
    End With
        
    For Each lilsUnlinked In lilsInlineShapes
        Set lrngUnlinked = lilsUnlinked.Range
        
        With lrngUnlinked
            Call .Cut
            Call .Paste
        End With
    Next lilsUnlinked
    
    Application.ScreenUpdating = lblnScreenUpdating

ProcFinally:
    Exit Sub

ProcCatch:
    Err.Clear
    On Error GoTo 0
    Resume ProcFinally

End Sub

Private Sub itfResizePicture_Delete()
    Call milsPicture.Delete
End Sub

Friend Function itfResizePicture_shpDuplicate(ByVal strTitleExpand_IN As String) As Shape
    Dim lReturn As Shape
    
    Dim lstrTitle As String
    
    With milsPicture
        lstrTitle = .Title  ' store previous title
        .Title = strTitleExpand_IN
        
        Set lReturn = basUtilities.InlineShape_shpDuplicate(milsPicture)
        
        .Title = lstrTitle  ' restore previous title
    End With
    
    Set itfResizePicture_shpDuplicate = lReturn
End Function

Friend Sub itfResizePicture_ShrinkToThumbnail(ByVal sngThumbnailWidth_IN As Single)
    If milsPicture.Width > sngThumbnailWidth_IN Then
        Call basUtilities.InlineShape_ShrinkToWidth(milsPicture, sngThumbnailWidth_IN)
    End If
End Sub

Private Function itfResizePicture_blnTitleEquals(ByVal strTitle_IN As String) As Boolean
    Dim lReturn As Boolean
    
    lReturn = basUtilities.InlineShape_blnTitleEquals(milsPicture, strTitle_IN)
    
    itfResizePicture_blnTitleEquals = lReturn
End Function
