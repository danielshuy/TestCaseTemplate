Attribute VB_Name = "basRibbonBackstage"
'Written by : Daniel Shuy

Option Explicit
Option Private Module   ' Visible only within this Project

Private Sub Refresh()
' ANY NEW CONTROL ADDED TO THE SETTINGS GROUP MUST BE ADDED HERE
' Resets controls to display Saved values
    Call basMain.mrbn.InvalidateControl("chkAutoUpdateTableOfContents")
    Call basMain.mrbn.InvalidateControl("edtShrinkToThumbnailWidth")
End Sub

Public Sub tabTestCaseTemplate_getVisible(ByVal ircTab_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    lReturn = False
    
    If basUtilities.Application_blnVersionSupported(basMain.mdblOFFICE_MINIMUM_VERSION) Then
        lReturn = True
    End If
    
    vntReturn_OUT = lReturn
End Sub

Public Sub tabTestCaseTemplate_getTitle(ByVal ircTab_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As String
    
    lReturn = "Test Case Template " & basMain.mdblVERSION
    
    vntReturn_OUT = lReturn
End Sub

Public Sub chkAutoUpdateTableOfContents_getPressed(ByVal ircCheckBox_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    
    Dim lblnEnabled As Boolean
    
    With basMain.getDocument
        lblnEnabled = .getAutoUpdateTableOfContents.isEnabled
        .getSettings.setAutoUpdateTableOfContents = lblnEnabled
    End With
    
    lReturn = lblnEnabled
    
    vntReturn_OUT = lReturn
End Sub

Public Sub chkAutoUpdateTableOfContents_onAction(ByVal ircCheckBox_IN As IRibbonControl, ByVal blnEnabled_IN As Boolean)
    basMain.getDocument.getSettings.setAutoUpdateTableOfContents = blnEnabled_IN
    
    Call ButtonsEnable(True)
End Sub

Public Sub edtShrinkToThumbnailWidth_getText(ByVal ircEditBox_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As String
    
    Dim ldblWidth As Double
    
    With basMain.getDocument
        ldblWidth = basUtilities.Math_dblRound(PointsToCentimeters(.getResizePicture.getThumbnailWidth), 2)
        .getSettings.setShrinkToThumbnailWidth = ldblWidth
    End With
    
    lReturn = ldblWidth
    
    vntReturn_OUT = lReturn
End Sub

Public Sub edtShrinkToThumbnailWidth_onChange(ByVal ircEditBox_IN As IRibbonControl, ByVal strInput_IN As String)
    
On Error GoTo ProcCatch
    basMain.getDocument.getSettings.setShrinkToThumbnailWidth = CDbl(strInput_IN)
On Error GoTo 0
    
    Call ButtonsEnable(True)
    
ProcFinally:
    Exit Sub

ProcCatch:
    Call Err.Clear
    On Error GoTo 0
    Call MsgBox("Width must be a Number")
    Call basMain.mrbn.InvalidateControl("edtShrinkToThumbnailWidth")    ' revert to current setting
    Resume ProcFinally

End Sub

Public Sub btnSave_getEnabled(ByVal ircButton_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    
    lReturn = basMain.getDocument.getSettings.getModified
    
    vntReturn_OUT = lReturn
End Sub

Public Sub btnSave_onAction(ByVal ircButton_IN As IRibbonControl)
    Call ButtonsEnable(False)
    
    ' persist settings
    With basMain.getDocument
        .getAutoUpdateTableOfContents.setEnabled = .getSettings.getAutoUpdateTableOfContents
        .getResizePicture.setThumbnailWidth = .getSettings.getShrinkToThumbnailWidth
    End With
    
    Call basMain.mrbn.InvalidateControl("edtShrinkToThumbnailWidth")    ' refresh EditBox to round Width to 2 decimal places
End Sub

Public Sub btnCancel_getEnabled(ByVal ircButton_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    
    lReturn = basMain.getDocument.getSettings.getModified
    
    vntReturn_OUT = lReturn
End Sub

Public Sub btnCancel_onAction(ByVal ircButton_IN As IRibbonControl)
    Call ButtonsEnable(False)
    
    Call Refresh
End Sub

Public Sub btnResetToDefaults_onAction(ByVal ircButton_IN As IRibbonControl)
    Call ButtonsEnable(False)
    
    With basMain.getDocument
        Call .getAutoUpdateTableOfContents.ResetToDefaults
        Call .getResizePicture.ResetToDefaults
    End With
    
    Call Refresh
End Sub

Private Sub ButtonsEnable(ByVal blnEnable_IN As Boolean)
    basMain.getDocument.getSettings.setModified = blnEnable_IN
    Call basMain.mrbn.InvalidateControl("btnSave")
    Call basMain.mrbn.InvalidateControl("btnCancel")
End Sub

