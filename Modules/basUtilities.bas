Attribute VB_Name = "basUtilities"
'Written by : Daniel Shuy

Option Explicit
Option Private Module   ' Visible only within this Project

Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)

Public Sub Application_Sleep(ByVal lngMilliseconds_IN As Long)
    Call Sleep(lngMilliseconds_IN)
End Sub

Public Function Application_blnVersionSupported(ByVal dblMinimumVersion_IN As Double) As Boolean
' Checks if current Application version is at least specified Minimum Supported Version
    Dim lReturn As Boolean
    lReturn = True
    
    If CDbl(Application.Version) < dblMinimumVersion_IN Then
        lReturn = False
    End If
    
    Application_blnVersionSupported = lReturn
End Function

Public Sub Array_Add(ByRef vntArray_INOUT As Variant, ByVal vntElement_IN As Variant)
    Dim lintIndex As Integer
    
    If IsArray(vntArray_INOUT) = False Then
        Call MsgBox("Error at basUtilities.Array_Add : vntArray_INOUT is not an array", vbCritical, "Error")
    End If
    
    If Not Array_blnIsDimensioned(vntArray_INOUT) Then
        lintIndex = 1
        ReDim vntArray_INOUT(1 To 1)
    Else
        lintIndex = UBound(vntArray_INOUT) + 1
        ReDim Preserve vntArray_INOUT(LBound(vntArray_INOUT) To lintIndex)
    End If
    
    vntArray_INOUT(lintIndex) = vntElement_IN
End Sub

Public Function Array_intGetDimensions(ByVal vntArray_IN As Variant) As Integer
'returns 0 if vntArray_IN is undimensioned
    Dim lReturn As Integer
    lReturn = 0
    
    Dim lintI As Integer
    lintI = 0
    
    If IsArray(vntArray_IN) = False Then
        Call MsgBox("Error at basUtilities.Array_intGetDimensions : vntArray_IN is not an array", vbCritical, "Error")
    End If
    
On Error GoTo ProcCatch
    Do
        lintI = lintI + 1
        lReturn = LBound(vntArray_IN, lintI)
    Loop
' no need to close On Error block as this loop will eventually fail

ProcFinally:
    Array_intGetDimensions = lReturn
    Exit Function

ProcCatch:
    Call Err.Clear
    On Error GoTo 0
    lReturn = lintI - 1
    Resume ProcFinally

End Function

Public Function Array_blnIsDimensioned(ByVal vntArray_IN As Variant) As Boolean
'vntArray_IN must be an array
    Dim lReturn As Boolean
    lReturn = True
    
    Dim lintI As Integer
    
    If IsArray(vntArray_IN) = False Then
        Call MsgBox("Error at basUtilities.Array_blnIsDimensioned : vntArray_IN is not an array", vbCritical, "Error")
    End If
    
    If UBound(vntArray_IN) < 0 Then
        lReturn = False
    End If

    Array_blnIsDimensioned = lReturn
End Function

Public Function Collection_blnContains(ByVal col_IN As Collection, ByVal strKey_IN As String) As Boolean
    Dim lReturn As Boolean
    lReturn = False
    
On Error GoTo ProcCatch
    Call col_IN.Item(strKey_IN)
    lReturn = True
On Error GoTo 0

ProcFinally:
    Collection_blnContains = lReturn
    Exit Function

ProcCatch:
    Call Err.Clear
    On Error GoTo 0
    Resume ProcFinally

End Function

Public Function Collection_vntGet(ByVal col_IN As Collection, ByVal strKey_IN As String) As Variant
    Dim lReturn As Variant
    
On Error GoTo ProcCatch
    lReturn = col_IN.Item(strKey_IN)
On Error GoTo 0

ProcFinally:
    Collection_vntGet = lReturn
    Exit Function

ProcCatch:
    Call Err.Clear
    On Error GoTo 0
    Resume ProcFinally
End Function

Public Function Dictionary_AddIfNotExist(ByRef dic_INOUT As Scripting.Dictionary, ByRef vntKey_INOUT As Variant, ByRef vntValue_INOUT As Variant) As Boolean
    Dim lReturn As Boolean
    
    lReturn = dic_INOUT.Exists(vntKey_INOUT)
    If Not lReturn Then
        Call dic_INOUT.Add(vntKey_INOUT, vntValue_INOUT)
    End If
    
    Dictionary_AddIfNotExist = lReturn
End Function

Public Function Dictionary_RemoveIfExist(ByRef dic_INOUT As Scripting.Dictionary, ByRef vntKey_INOUT As Variant) As Boolean
    Dim lReturn As Boolean
    
    lReturn = dic_INOUT.Exists(vntKey_INOUT)
    If lReturn Then
        Call dic_INOUT.Remove(vntKey_INOUT)
    End If
    
    Dictionary_RemoveIfExist = lReturn
End Function

Public Sub DocumentVariables_Clear(ByVal doc_IN As Document)
    Dim lvrb As Variable
    
    With doc_IN
        For Each lvrb In .Variables
            Call .Variables.Item(lvrb.Name).Delete
        Next lvrb
    End With
End Sub

Public Function DocumentVariables_blnContains(ByVal doc_IN As Document, ByVal strKey_IN As String) As Boolean
    Dim lReturn As Boolean
    lReturn = False
    
    Dim lstr As String
    
On Error GoTo ProcCatch
    lstr = doc_IN.Variables(strKey_IN)
    lReturn = True
On Error GoTo 0

ProcFinally:
    DocumentVariables_blnContains = lReturn
    Exit Function

ProcCatch:
    Call Err.Clear
    On Error GoTo 0
    Resume ProcFinally

End Function

Public Function InlineShape_shpDuplicate(ByRef ils_IN As InlineShape) As Shape
    Dim lReturn As Shape
    
    Dim lshpPicture As Shape
    
    Set lshpPicture = basUtilities.InlineShape_shpToShape(ils_IN)
    
    Set lReturn = lshpPicture.Duplicate
    
    Set ils_IN = lshpPicture.ConvertToInlineShape
    
    Set InlineShape_shpDuplicate = lReturn
End Function

Public Sub InlineShape_ShrinkToWidth(ByRef ils_IN As InlineShape, ByVal sngWidth_IN As Single)
    Dim lblnLockAspectRatio As Boolean
    
    With ils_IN
        lblnLockAspectRatio = .LockAspectRatio  ' store previous LockAspectRatio flag
        
        .LockAspectRatio = True
        .Width = sngWidth_IN
        
        .LockAspectRatio = lblnLockAspectRatio  ' restore previous LockAspectRatio flag
    End With
End Sub

Public Function InlineShape_blnTitleEquals(ByRef ils_IN As InlineShape, ByVal strTitle_IN As String) As Boolean
    Dim lReturn As Boolean
    
    If ils_IN.Type = wdInlineShapeEmbeddedOLEObject Then    ' avoid Run-time error 445: Object doesn't support this action for Embedded Excel Sheet
        lReturn = False
    Else
        lReturn = (ils_IN.Title = strTitle_IN)
    End If
    
    InlineShape_blnTitleEquals = lReturn
End Function

Public Function InlineShape_shpToShape(ByRef ils_IN As InlineShape) As Shape
' VBA has a weird bug which resets Scaling when converting an Inline Shape that is too small (~<30%) to a Shape
' Use this method instead of InlineShape.ConvertToShape()
' FIX: Store previous size, reset Scaling, convert to Shape, then restore previous size
    Dim lReturn As Shape
    
    Dim lsngHeight As Single
    Dim lsngWidth As Single
    
    With ils_IN
        lsngHeight = .Height    ' store previous height
        lsngWidth = .Width  ' store previous width
        
        ' reset Scaling
        .ScaleHeight = 100
        .ScaleWidth = 100
    
        Set lReturn = .ConvertToShape
    End With
    
    lReturn.Height = lsngHeight ' restore previous height
    lReturn.Width = lsngWidth   ' restore previous width
    
    Set InlineShape_shpToShape = lReturn
End Function

Public Function Math_dblConstPI() As Double
    Dim lReturn As Double

    lReturn = 4 * Atn(1)
    
    Math_dblConstPI = lReturn
End Function

Public Function Math_dblDegToRad(ByVal dblDegrees_IN As Double) As Double
    Dim lReturn As Double
    
    lReturn = dblDegrees_IN / 180 * Math_dblConstPI
    
    Math_dblDegToRad = lReturn
End Function

Public Function Math_dblMin(ParamArray vntNumbers() As Variant) As Double
'Takes in a vararg of numbers and returns the smallest value specified
    Dim lReturn As Double
    
    Dim lintI As Integer
    Dim ldblNumber As Double
    
    lReturn = vntNumbers(LBound(vntNumbers))
    
    If LBound(vntNumbers) <> UBound(vntNumbers) Then
        For lintI = LBound(vntNumbers) + 1 To UBound(vntNumbers)
            ldblNumber = CDbl(vntNumbers(lintI))
            If ldblNumber < lReturn Then
                lReturn = ldblNumber
            End If
        Next lintI
    End If
    
    Math_dblMin = lReturn
End Function

Public Function Math_dblRadToDeg(ByVal dblRadians_IN As Double) As Double
    Dim lReturn As Double
    
    lReturn = dblRadians_IN / Math_dblConstPI * 180
    
    Math_dblRadToDeg = lReturn
End Function

Public Function Math_dblRound(ByVal dblNumber_IN As Double, ByVal lngNumDigitsAfterDecimal_IN As Long)
' Performs arithmetic rounding
    Dim lReturn As Double
    
    Dim llng As Long
    
    llng = (10 ^ lngNumDigitsAfterDecimal_IN)
    lReturn = CInt(dblNumber_IN * llng) / llng
    
    Math_dblRound = lReturn
End Function

Public Function Shape_shpDuplicate(ByRef shp_IN As Shape) As Shape
' VBA has a weird bug which will cause Word to be Not-responding when duplicating a Shape within a table
' Use this method instead of Shape.Duplicate()
' FIX: Convert Shape to InlineShape, duplicate it, then convert it back to a Shape
    Dim lReturn As Shape
    
    Dim llngZOrderPosition As Long
    Dim lsngLeft As Single
    Dim lsngTop As Single
    Dim lmstType As MsoShapeType
    Dim lils As InlineShape
    
    With shp_IN
        llngZOrderPosition = .ZOrderPosition    ' store previous Z Order Position
        
        ' store previous position
        lsngLeft = .Left
        lsngTop = .Top
        
        lmstType = .WrapFormat.Type ' store previous Layout Option (Wrap Text)
        
        Set lils = shp_IN.ConvertToInlineShape
        Set lReturn = basUtilities.InlineShape_shpDuplicate(lils)
        
        Set shp_IN = basUtilities.InlineShape_shpToShape(lils)
        
        .WrapFormat.Type = lmstType ' restore previous Layout Option (Wrap Text)
        
        ' restore previous position
        .Left = lsngLeft
        .Top = lsngTop
        
        ' restore previous Z Order Position
        Do While .ZOrderPosition > llngZOrderPosition
            Call .ZOrder(msoSendBackward)
        Loop
    End With
    
    Set Shape_shpDuplicate = lReturn
End Function

Public Sub Shape_ShrinkToWidth(ByRef shp_IN As Shape, ByVal sngWidth_IN As Single)
    Dim lblnLockAspectRatio As Boolean
    Dim lsngHeight As Single
    
    With shp_IN
        lblnLockAspectRatio = .LockAspectRatio  ' store previous LockAspectRatio flag
        lsngHeight = .Height    ' store previous Height
        
        ' retain centre point after resizing
        .Left = .Left + ((.Width - sngWidth_IN) / 2)
        .LockAspectRatio = True
        .Width = sngWidth_IN
        .Top = .Top + ((lsngHeight - .Height) / 2)
        
        .LockAspectRatio = lblnLockAspectRatio  ' restore previous LockAspectRatio flag
    End With
End Sub

Public Function Shape_blnTitleEquals(ByRef shp_IN As Shape, ByVal strTitle_IN As String) As Boolean
    Dim lReturn As Boolean
    
    lReturn = (shp_IN.Title = strTitle_IN)
    
    Shape_blnTitleEquals = lReturn
End Function

Public Function String_blnContains(ByVal strExpression_IN As String, ByVal strFind_IN As String) As Boolean
    Dim lReturn As Boolean

    lReturn = InStr(1, strExpression_IN, strFind_IN) > 0

    String_blnContains = lReturn
End Function

