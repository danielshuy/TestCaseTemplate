Attribute VB_Name = "basRibbonContextMenu"
'Written by : Daniel Shuy

Option Explicit
Option Private Module   ' Visible only within this Project

Public Sub btnShrinkToThumbnail_getVisible(ByVal ircButton_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    lReturn = False
    
    If basUtilities.Application_blnVersionSupported(basMain.mdblOFFICE_MINIMUM_VERSION) Then
        lReturn = True
    End If
    
    vntReturn_OUT = lReturn
End Sub

Public Sub btnShrinkToThumbnail_Click(ByVal ircButton_IN As IRibbonControl)
    Call basMain.getDocument.getResizePicture.ShrinkToThumbnail
End Sub

Public Sub btnExpandToOriginalSize_getVisible(ByVal ircButton_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    lReturn = False
    
    Dim lclsResizePicture As itfResizePicture
    
    If basUtilities.Application_blnVersionSupported(basMain.mdblOFFICE_MINIMUM_VERSION) Then
        ' don't display Button if more than 1 Picture is selected
        ' don't display Button if selected Picture is Expanded
        With Application.Selection
            If .InlineShapes.Count = 1 Then
                If .ShapeRange.Count = 0 Then
                    If Not basMain.getDocument.getResizePicture.blnExpanded(.InlineShapes(1)) Then
                        lReturn = True
                    End If
                End If
            ElseIf .ShapeRange.Count = 1 Then
                If .InlineShapes.Count = 0 Then
                    If Not basMain.getDocument.getResizePicture.blnExpanded(.ShapeRange(1)) Then
                        lReturn = True
                    End If
                End If
            End If
        End With
    End If
    
    vntReturn_OUT = lReturn
End Sub

Public Sub btnExpandToOriginalSize_Click(ByVal ircButton_IN As IRibbonControl)
    Call basMain.getDocument.getResizePicture.ExpandToOriginalSize
End Sub

Public Sub btnExpandToFitPage_getVisible(ByVal ircButton_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    lReturn = False
    
    If basUtilities.Application_blnVersionSupported(basMain.mdblOFFICE_MINIMUM_VERSION) Then
        ' don't display Button if more than 1 Picture is selected
        ' don't display Button if selected Picture is Expanded
        With Application.Selection
            If .InlineShapes.Count = 1 Then
                If .ShapeRange.Count = 0 Then
                    If Not basMain.getDocument.getResizePicture.blnExpanded(.InlineShapes(1)) Then
                        lReturn = True
                    End If
                End If
            ElseIf .ShapeRange.Count = 1 Then
                If .InlineShapes.Count = 0 Then
                    If Not basMain.getDocument.getResizePicture.blnExpanded(.ShapeRange(1)) Then
                        lReturn = True
                    End If
                End If
            End If
        End With
    End If
    
    vntReturn_OUT = lReturn
End Sub

Public Sub btnExpandToFitPage_Click(ByVal ircButton_IN As IRibbonControl)
    Call basMain.getDocument.getResizePicture.ExpandToFitPage
End Sub

Public Sub btnCancelExpand_getVisible(ByVal ircButton_IN As IRibbonControl, ByRef vntReturn_OUT As Variant)
    Dim lReturn As Boolean
    lReturn = False
    
    Dim lils As InlineShape
    Dim lshp As Shape
    
    If basUtilities.Application_blnVersionSupported(basMain.mdblOFFICE_MINIMUM_VERSION) Then
        ' only display Button if at least 1 selected Picture is Expanded
        
        For Each lils In Application.Selection.InlineShapes
            If basMain.getDocument.getResizePicture.blnExpanded(lils) Then
                lReturn = True
                Exit For
            End If
        Next lils
        
        If Not lReturn Then
            For Each lshp In Application.Selection.ShapeRange
                If basMain.getDocument.getResizePicture.blnExpanded(lshp) Then
                    lReturn = True
                    Exit For
                End If
            Next lshp
        End If
    End If
    
    vntReturn_OUT = lReturn
End Sub

Public Sub btnCancelExpand_Click(ByVal ircButton_IN As IRibbonControl)
    Call basMain.getDocument.getResizePicture.CancelExpand
End Sub

