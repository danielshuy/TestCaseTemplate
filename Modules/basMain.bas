Attribute VB_Name = "basMain"
'Written by : Daniel Shuy

Option Explicit
Option Private Module   ' Visible only within this Project

Public Const mdblVERSION As Double = 10.4
Public Const mdblOFFICE_MINIMUM_VERSION As Double = 14

Public mrbn As IRibbonUI

Private mdicDocuments As Scripting.Dictionary
Private mclsApplication As clsApplication

Public Sub Document_EditPaste()
' if Pasted picture is copied from Paint (PBrush), convert it to a normal picture
    Dim llngRangeStart As Long
    Dim lund As UndoRecord
    Dim llngRangeEnd As Long
    Dim lrng As Range
    Dim lils As InlineShape
    Dim lshp As Shape
    Dim lclsResizePicture As itfResizePicture
    
    llngRangeStart = Selection.Range.Start
    
    Set lund = Application.UndoRecord
    Call lund.StartCustomRecord("Paste")
    
    Call Selection.Paste
    
    llngRangeEnd = Selection.Range.End
    
    Set lrng = ActiveDocument.Range(llngRangeStart, llngRangeEnd)
    
    With lrng
        For Each lils In .InlineShapes
            Set lclsResizePicture = basConstructors.newResizePictureInlineShape(lils)
            Call lclsResizePicture.ConvertPBrushToPicture
        Next lils
        
        For Each lshp In .ShapeRange
            Set lclsResizePicture = basConstructors.newResizePictureShape(lshp)
            Call lclsResizePicture.ConvertPBrushToPicture
        Next lshp
    End With
    
    Call lund.EndCustomRecord
End Sub

Public Sub Ribbon_onLoad(ByVal rbn_IN As IRibbonUI)
    Dim ltmp As Template
    
    Set mrbn = rbn_IN
    
    If basUtilities.Application_blnVersionSupported(mdblOFFICE_MINIMUM_VERSION) Then
        Set mdicDocuments = New Scripting.Dictionary
        
        ' store reference to template
        Set ltmp = Application.ActiveDocument.AttachedTemplate
        
        Set mclsApplication = basConstructors.newApplication(ltmp.FullName)
        
        Call setDocument
    End If
End Sub

Public Sub setDocument(Optional ByRef doc_INOUT As Document)
    If doc_INOUT Is Nothing Then
        Set doc_INOUT = Application.ActiveDocument
    End If
    
    Call mdicDocuments.Add(doc_INOUT.FullName, basConstructors.newDocument(doc_INOUT))
End Sub

Public Function getDocument(Optional ByVal doc_IN As Document) As clsDocument
    Dim lReturn As clsDocument
    
    Dim lstrPath As String
    
    If doc_IN Is Nothing Then
        Set doc_IN = Application.ActiveDocument
    End If
    
    lstrPath = doc_IN.FullName
    
    If mdicDocuments.Exists(lstrPath) Then
        Set lReturn = mdicDocuments(lstrPath)
    End If
    
    Set getDocument = lReturn
End Function

