Attribute VB_Name = "basConstructors"
'Written by : Daniel Shuy

'Factory class

Option Explicit
Option Private Module   ' Visible only within this Project

Public Function newApplication(ByVal strPathTemplate_IN As String) As clsApplication
    Dim lReturn As New clsApplication
    
    Call lReturn.Constructor(strPathTemplate_IN)
    
    Set newApplication = lReturn
End Function

Public Function newDocument(ByRef doc_INOUT As Document) As clsDocument
    Dim lReturn As New clsDocument
    
    Call lReturn.Constructor(doc_INOUT)
    
    Set newDocument = lReturn
End Function

Public Function newSettings(ByRef clsDocument_INOUT As clsDocument) As clsSettings
    Dim lReturn As New clsSettings
    
    Call lReturn.Constructor(clsDocument_INOUT)
    
    Set newSettings = lReturn
End Function

Public Function newAutoUpdateTableOfContents(ByRef clsDocument_INOUT As clsDocument) As clsAutoUpdateTableOfContents
    Dim lReturn As New clsAutoUpdateTableOfContents
    
    Call lReturn.Constructor(clsDocument_INOUT)
    
    Set newAutoUpdateTableOfContents = lReturn
End Function

Public Function newResizePicture(ByRef clsDocument_INOUT As clsDocument) As clsResizePicture
    Dim lReturn As New clsResizePicture
    
    Call lReturn.Constructor(clsDocument_INOUT)
    
    Set newResizePicture = lReturn
End Function

Public Function newResizePictureInlineShape(ByRef ilsPicture_INOUT As InlineShape) As clsResizePictureInlineShape
    Dim lReturn As New clsResizePictureInlineShape
    
    Call lReturn.Constructor(ilsPicture_INOUT)
    
    Set newResizePictureInlineShape = lReturn
End Function

Public Function newResizePictureShape(ByRef shpPicture_INOUT As Shape) As clsResizePictureShape
    Dim lReturn As New clsResizePictureShape
    
    Call lReturn.Constructor(shpPicture_INOUT)
    
    Set newResizePictureShape = lReturn
End Function

